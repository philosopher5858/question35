<!DOCTYPE html>
<html lang="en">
<head>
   <style>
 .header{
   position:relative;
   width: 1780px;
   height: 150px;
   border-radius: 10px;
   padding: 40px;
   font-size: 17px;
   box-sizing: border-box;
   background:#ecf0f3;
   box-shadow: 14px 14px 20px #cbced1, -14px -14px 20px white;
   letter-spacing: 2px;
   
 }
 
 body {
   margin: 0;
   width: 100vw;
   height: 100vh;
   background: #ecf0f3;
   display: flex;
   align-items: center;
   text-align: center;
   justify-content: center;
   place-items: center;
   overflow: hidden;
   font-family: 'Courier New', Courier, monospace;
 }
 
 .container {
   position: relative;
   width: 500px;
   height: 150px;
   border-radius: 20px;
   padding: 40px;
   /* box-sizing: border-box; */
   background:#ecf0f3;
   box-shadow: 14px 14px 20px #cbced1, -14px -14px 20px white;
   letter-spacing: 4px;
   
 }
  
 .container1 {
   position: relative;
   width: 500px;
   height: 150px;
   border-radius: 20px;
   padding: 40px;
   /* box-sizing: border-box; */
   background:#ecf0f3;
   box-shadow: 14px 14px 20px #cbced1, -14px -14px 20px white;
   letter-spacing: 4px;
   
 }
 
 .inputs {
   text-align: left;
   margin-top: 3px;
 }
 
 label, input, button {
   display:inline;
   width: 100%;
   padding: 0;
   border: none;
   outline: none;
   box-sizing: border-box;
 }
 .lable2{
   margin-bottom: 40px;
   padding-right: 100%;
   font-family: 'Courier New', Courier, monospace;
 }
 
 label {
   margin-bottom: 40px;
   padding-right: 100%;
   font-family: 'Courier New', Courier, monospace;
 }
 
 label:nth-of-type(2) {
   margin-top: 12px;
   padding-right: 0%;
 }
 
 input::placeholder {
   color: gray;
 }
 
 input {
   background: #f7f9fb;
   padding: 10px;
   padding-left: 20px;
   height: 50px;
   font-size: 14px;
   border-radius: 20px;
   box-shadow: inset 6px 6px 6px #7da9d6, inset -6px -6px 6px rgb(205, 246, 249);
 }
 
 button {
   color: white;
   margin-top: 20px;
   background: #1DA1F2;
   height: 40px;
   width: 220px;
   border-radius: 20px;
   cursor: pointer;
   font-weight: 900;
   box-shadow: 6px 6px 6px #cbced1, -6px -6px 6px white;
   transition: 0.5s;
 }
 
 button:hover {
   box-shadow: none;
 }
 
 a {
   position: absolute;
   font-size: 8px;
   bottom: 4px;
   right: 4px;
   text-decoration: none;
   color: black;
   background: yellow;
   border-radius: 10px;
   padding: 2px;
 }
 
 h1 {
   position: absolute;
   top: 0;
   left: 0;
 }
 
   </style>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Document</title>
</head>
<body>
   <form action="Question35.html" method="POST">
   <div class="header">
     QUESTION 35: <br><br>	WAP to print the following series:
     i.	10,9,8,…..1 <br>
     ii.	2,4,6,8,…..20 <br>
     iii.	10,13.5,17,20.5 <br>
   </div>
   <br> <br>
   <table>
    <td>
       <DIv class="container">
                       <?php
if($_SERVER['REQUEST_METHOD']=='POST'){
    $i = 10;
    while($i>=1){
        print "<b>".$i.",";
        $i--;
    }//
    ?>
    </div>
</td>
    <td>
    <?php
  echo '<div class="container1">';
    $i = 2;
    while($i<=20){
        if($i%2==0){
            print "<b>".$i.",";
        }
        $i++;
    }//
    echo '</div>';
}
    ?>
    </td>
    <td>
    <div class="container">
<?php
  $i = 1;
  $l = 0;
  while($i<=4){
    if($i==$i){
        $l = 10 + ($i-1)*3.5;
        print $l.",";
    }
      $i++;
  }

 
?>
</div>
</td>        
     </table>
     <button type="submit">BACK</button>
   </form>
</body>
</html>